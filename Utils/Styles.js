import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	texto_link: {
		color: '#6666FF',
 		fontWeight: 'bold'
	},
	texto: {
		fontSize: 17,
		color: '#808585'

	}
});
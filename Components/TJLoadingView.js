import { Modal, View, Text } from 'react-native';
import React, { Component } from 'react';

export default class TJLoadingView extends Component {
    render() {
        return (
        	<Modal visible transparent >
        		<View style={Style.modalBackground}>
              <View style={Style.view_loading}>
                  <Text style={Style.texto} > Carregando ...</Text>
              </View>
        		</View>
        	</Modal>
        );
    }
}

const Style = {
	modalBackground: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'space-around',
		backgroundColor: '#00000040'
  },
  texto: {
  	fontSize: 20,
  	fontWeight: 'bold',
  	textAlign: 'center',
  	color: 'grey'
  },
  view_loading: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 50
  }
};


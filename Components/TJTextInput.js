import { View, TextInput, Text, TouchableHighlight, Image } from 'react-native';
import React, { Component } from 'react';
import stylesPadrao from '../Utils/Styles';

export default class TJTextInput extends Component {
	constructor(props) {
		super(props);

		this.state = {
			secureText: true
		};
	}

	onChangeText(text) {
		this.props.atualizaDados(this.props.state, text);
		if (this.props.onChangeText != null) {
			this.props.onChangeText(text);
		}
	}

	exibirSenha() {
		this.setState({ secureText: !this.state.secureText });
	}

    render() {
        return (
			<View style={this.props.style === null ? Style.view : this.props.style}> 
				<Text style={stylesPadrao.texto}>{this.props.texto}</Text>
				<View style={Style.view_text_field}>
					<TextInput 
						style={Style.text_input} 
						onChangeText={text => this.onChangeText(text)} 
						value={this.props.value} 
						autoCapitalize="none"
						keyboardType={this.props.keyboardType != null ? this.props.keyboardType : 'default'}
						secureTextEntry={this.props.secureTextEntry != null ? this.state.secureText : null}
						/> 
						{ this.props.secureTextEntry != null ? 
							<View style={Style.view_touch}>
								<TouchableHighlight activeOpacity={0.6} underlayColor="#DDDDDD" onPress={() => { this.exibirSenha(); }}> 
									<Image style={Style.image} source={ this.state.secureText ? require('../Imagens/eye_on.png') : require('../Imagens/eye_off.png')} />
								</TouchableHighlight>
							</View>
							:
							null
						}
				</View>

				
			</View>
        );
    }
}


const Style = {
	text_input: {
        height: 40,
        flex: 1
	},
	view: {
		alignSelf: 'stretch'
	},
	image: {
		width: 30,
		height: 30,
		marginRight: 10
	},
	view_text_field: {
		flexDirection: 'row',
		borderBottomColor: '#000000',
        borderWidth: 0.5,
        borderRadius: 5,
	},
	view_touch: {
		alignItems: 'center',
		justifyContent: 'center'
	}
};

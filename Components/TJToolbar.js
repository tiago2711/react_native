import { View, Text } from 'react-native';
import React, { Component } from 'react';
import stylesPadrao from '../Utils/Styles';

export default class TJToolbar extends Component {
    render() {
        return (
			<View style={Style.view}> 
				<Text style={Style.titulo} > {this.props.titulo} </Text>
			</View>
        );
    }
}


const Style = {
	view: {
		backgroundColor: '#508EFF',
		alignSelf: 'stretch',
		height: 60,
		justifyContent: 'center'
	},

	titulo: {
		fontWeight: 'bold',
		color: '#FDFFFE',
		fontSize: 25,
		alignSelf: 'stretch',
		textAlign: 'center'
	}

};

import { View, Button } from 'react-native';
import React, { Component } from 'react';

export default class TJAcessButton extends Component {
    render() {
        return (
			<View style={Style.view}> 
				<Button style={Style.button} title={this.props.titulo} onPress={this.props.onClick} disabled={this.props.disabled} />
			</View>
        );
    }
}

const Style = {
	button: {
		borderRadius: 10
	},

	view: {
		alignSelf: 'stretch',
		paggin: 20,
		paddingLeft: 30,
		paddingRight: 30,
		paddingBottom: 30
	}
};

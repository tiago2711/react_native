import { View, Image, Text, Alert } from 'react-native';
import React from 'react';
import auth from '@react-native-firebase/auth';
import TJComponents from '../../Components/TJComponents';
import TJTextInput from '../../Components/TJTextInput';
import TJAcessButton from '../../Components/TJAcessButton';
import TJLoadingView from '../../Components/TJLoadingView';
import styles from './Style';
import stylesPadrao from '../../Utils/Styles';


export default class Splash extends TJComponents {
	constructor(props) {
		super(props);

		this.state = {
			usuario: '',
			senha: '',
			loading: false
		};

		this.atualizaDados = this.atualizaDados.bind(this);
		this.verificarCampos = this.verificarCampos.bind(this);
	}
	verificarCampos() {
		if (!(this.state.usuario.trim() === '') && !(this.state.senha.trim() === '')) {
			this.setState({ loading: true });
			auth().signInWithEmailAndPassword(this.state.usuario, this.state.senha).then(() => {
				this.setState({ loading: false });
			})
			.catch(error => {
				this.setState({ loading: false });
				Alert.alert(
				'Atenção !',
				'Usuário ou senha estão incorretos',
				[
				{ text: 'OK' }
				],
				{ cancelable: false }
			);
			});
		} else {
			Alert.alert(
				'Atenção !',
				'Digite um usuário e senha valida.',
				[
				{ text: 'OK' }
				],
				{ cancelable: false }
			);
		}
	}
    render() {
        return (
			<View style={styles.view} > 
				{this.state.loading ? <TJLoadingView /> : null}  
				<Image style={styles.image_logo} source={require('../../Imagens/logo_login.png')}/>
				<Text style={stylesPadrao.texto}> Entrar </Text>

				<View style={styles.view_campos}> 
					<TJTextInput texto="Usuário: " atualizaDados={this.atualizaDados} state="usuario" />
					<TJTextInput texto="Senha: " atualizaDados={this.atualizaDados} state="senha" secureTextEntry={true}/> 
					<Text style={styles.text_esqueceu_senha}> </Text>
				</View>

				<TJAcessButton titulo="Acessar" onClick={this.verificarCampos} />

				<TJAcessButton titulo="Cadastrar" disabled />
			</View>
        );
    }
}

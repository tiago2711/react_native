import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	view: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},

	image_logo: {
		height: 200,
		width: 200
	},

	view_campos: {
		alignSelf: 'stretch',
		padding: 30
	},
	text_esqueceu_senha: {
		color: '#6666FF',
		textAlign: 'right',
		marginTop: 10
	}
});
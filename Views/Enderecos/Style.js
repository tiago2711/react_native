import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	view: {
		flex: 1,
		alignItems: 'stretch',
		justifyContent: 'center'
	},
	image_plus: {
		height: 50,
		width: 50,
		alignSelf: 'center'
	},
	button_plus: {
		alignSelf: 'center'
	},
	item: {
		borderBottomColor: '#000000',
        borderWidth: 0.5,
		padding: 20
	}
});

import { View, Button, Keyboard } from 'react-native';
import React from 'react';
import axios from 'axios';
import styles from './Style';

import { Actions } from 'react-native-router-flux';

import TJTextInput from '../../../Components/TJTextInput';
import TJComponents from '../../../Components/TJComponents';
import TJLoadingView from '../../../Components/TJLoadingView';

export default class NovoEndereco extends TJComponents {
	constructor(props) {
		super(props);

		this.state = {
			cep: '',
			rua: '',
			numero: '',
			cidade: '',
			estado: '',
			loading: false
		};

		this.atualizaDados = this.atualizaDados.bind(this);
	}

	componentDidMount() {
		if (this.props.editar) {
			const endereco = this.props.endereco;
			this.setState({ cep: endereco.cep });
			this.setState({ numero: endereco.numero });
			this.setState({ rua: endereco.rua });
			this.setState({ cidade: endereco.cidade });
			this.setState({ estado: endereco.estado });
		}
	}

	consultarCep() {
		Keyboard.dismiss();
		if (this.state.cep.length >= 8) {
			this.setState({ loading: true });
			axios.get('http://viacep.com.br/ws/' + this.state.cep + '/json/')
			.then(response => { 
				this.setState({ loading: false });
				const data = response.data;
				this.setState({ cep: data.cep });
				this.setState({ rua: data.logradouro });
				this.setState({ cidade: data.localidade });
				this.setState({ estado: data.uf });
			});
		} else {
			alert('Digite um CEP válido.');
		}
	}

	salvarEndereco() {
		const endereco = {
			id: this.props.id_endereco,
			cep: this.state.cep,
			rua: this.state.rua,
			numero: this.state.numero,
			cidade: this.state.cidade,
			estado: this.state.estado
		};

		this.props.novoEndereco(endereco);
		Actions.pop();
	}

    render() {
        return (      	
			<View style={styles.view}> 
					{this.state.loading ? <TJLoadingView /> : null}  
					<View style={styles.view_cep}>
						<TJTextInput texto="CEP: " atualizaDados={this.atualizaDados} style={styles.text_cep} state="cep" value={this.state.cep} keyboardType={'numeric'} />
						<View style={styles.button_cep}>
							<Button title="Buscar" onPress={() => this.consultarCep()} />
						</View>
					</View>
					<TJTextInput texto="RUA: " atualizaDados={this.atualizaDados} state="rua" value={this.state.rua} /> 
					<TJTextInput texto="Número: " atualizaDados={this.atualizaDados} state="numero" value={this.state.numero} keyboardType={'numeric'} /> 
					<TJTextInput texto="Cidade: " atualizaDados={this.atualizaDados} state="cidade" value={this.state.cidade} /> 
					<TJTextInput texto="Estado: " atualizaDados={this.atualizaDados} state="estado" value={this.state.estado} /> 
					<View style={styles.button_salvar}>
						<Button title="Salvar Endereço" onPress={() => this.salvarEndereco()} />
					</View>
			</View>
        );
    }
}


import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	view: {
		flex: 1,
		padding: 20,
		flexDirection: 'column'
	},
	list: {
		backgroundColor: '#D0D4D0'
	},
	image_plus: {
		height: 50,
		width: 50,
		alignSelf: 'flex-end'
	},
	view_cep: {
		flexDirection: 'row'
	},
	text_cep: {
		flex: 3
	},
	button_cep: {
		flex: 1,
		justifyContent: 'flex-end'
	},
	button_salvar: {
		flex: 1,
		justifyContent: 'flex-end'
	}
});
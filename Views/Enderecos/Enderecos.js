import { View, FlatList, Image, TouchableHighlight, Text, Alert } from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import styles from './Style';

export default class Enderecos extends Component {
	constructor(props) {
		super(props);

		this.state = {
			enderecos: [],
			id_enredecos: 0
		};

		this.novoEndereco = this.novoEndereco.bind(this);
	}

	novoEndereco(endereco) {
		const novoEndereco = this.state.enderecos.find(element => element.id === endereco.id);
		if (novoEndereco != null) {
			novoEndereco.cep = endereco.cep;
			novoEndereco.numero = endereco.numero;
			novoEndereco.rua = endereco.rua;
			novoEndereco.cidade = endereco.cidade;
			novoEndereco.estado = endereco.estado;

			this.setState({ enderecos: this.state.enderecos });
		} else {	
			this.setState({ id_enredecos: this.state.id_enredecos + 1 });
			const joined = this.state.enderecos.concat(endereco);
			this.setState({ enderecos: joined });
		}
	}

	selecionarItem(item) {
		Alert.alert(
			'Atenção',
			'Você deseja deletar ou editar o endereço? ',
			[{
				text: 'Deletar',
				onPress: () => {
					const novo = this.state.enderecos.filter(function(obj) {
						return obj.id !== item.id;
					});
					this.setState({ enderecos: novo });
				}
			},
			{ text: "Editar", onPress: () => {
				const id = item.id;
				const endereco = item;
				Actions.novoEndereco({ novoEndereco: this.novoEndereco, id_endereco: id, editar: true, endereco: endereco });
			} 
			}],
			{ cancelable: true }
	   	);
	}

    render() {
        return (      	
			<View style={styles.view}> 
				<FlatList 
		          	data={this.state.enderecos}
		          	renderItem={({ item }) => {
			            return (
			              <View>
			              <TouchableHighlight onPress={() => { this.selecionarItem(item); }} activeOpacity={0.6} underlayColor="#DDDDDD" >
				              <View style={styles.item}>
				              		<Text>{item.rua}</Text>
					                <Text>{item.cidade}</Text>
					                <Text>{item.estado}</Text>
				              </View> 
			                </TouchableHighlight >
			              </View>
			            );
		          	} 
		     	 }
		        />
				<TouchableHighlight onPress={() => { Actions.novoEndereco({ novoEndereco: this.novoEndereco, id_endereco: this.state.id_enredecos }); }} >
					<Image style={styles.image_plus} source={require('../../Imagens/plus.png')} />
				</TouchableHighlight >

			</View>
        );
    }
}

import { View, Image, Text, TouchableHighlight, Alert } from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import auth from '@react-native-firebase/auth';

import styles from './Style';


export default class Inicio extends Component {
	sair() {
		Alert.alert(
			'Atenção',
			'Você realmente deseja sair?',
			[{
				text: 'Sim',
				onPress: () => {
					auth().signOut().then(() => {});
				}					
			},
			{ text: "Não", onPress: () => {} 
			}],
			{ cancelable: true });
	}

    render() {
        return (      	
			<View style={styles.superView}> 
				<View style={styles.subView}>
					<TouchableHighlight activeOpacity={0.6} underlayColor="#DDDDDD" onPress={() => { Actions.enderecos(); }}> 
						<View style={styles.view_opcoes}>
							<Image style={styles.imagens} source={require('../../Imagens/endereco.png')} />
							<Text>Endereço</Text>
						</View>
					</TouchableHighlight>

					<TouchableHighlight activeOpacity={0.6} underlayColor="#DDDDDD" onPress={() => {this.sair();} }> 
						<View style={styles.view_opcoes}>
							<Image style={styles.imagens} source={require('../../Imagens/exit.png')} />
							<Text>Sair</Text>
						</View>
					</TouchableHighlight>

				</View>
			</View>
        );
    }
}

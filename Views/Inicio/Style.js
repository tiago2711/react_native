import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	superView: {
		flex: 1,
		alignItems: 'stretch'
	},
	subView: {
		flex: 1,
		justifyContent: 'space-around',
		alignItems: 'center',
		padding: 20
	},
	imagens: {
		height: 100,
		width: 100
	},
	view_opcoes: {
		backgroundColor: '#D0D4D0',
		padding: 5,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 10
	}
});

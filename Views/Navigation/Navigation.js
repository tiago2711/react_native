import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import Enderecos from '../Enderecos/Enderecos';
import NovoEndereco from '../Enderecos/NovoEndereco/NovoEndereco';
import Inicio from '../Inicio/Inicio';

export default class Navigation extends Component {
    render() {
        return (      	
			<Router>
				<Scene key="root">
			      <Scene key='inicio' component={Inicio} initil title='Inicio' />
			      <Scene key='enderecos' component={Enderecos} title='Endereços' />
			      <Scene key='novoEndereco' component={NovoEndereco} title='Novo Endereço' />
			   </Scene>
			</Router>
        );
    }
}

# react-native

# Projeto de estudo sobre react-native.

######  Pontos abordados:

:x: Layout básicos de fluxo de login e cadastro. <br />
:heavy_check_mark: Configuração Firebase. <br />
:heavy_check_mark: Lógica de login utilizando Firebase Auth. <br />
:x: Utilização de persistência de dados no aparelho. <br />
:heavy_check_mark: Consumo de APIs. <br />
:heavy_check_mark: Estruturas dinâmicas. <br />
:x: Configuração/Utilização de banco de dados no aparelho. <br />

Versão v1.0.0 <br /> 
Android: [APK](https://drive.google.com/file/d/1BloAAuAkCqMm6_CG2QVikJn2sXvnbxdn/view?usp=sharing)
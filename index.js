/**
 * @format
 */
import React, { useState, useEffect } from 'react';
import { AppRegistry } from 'react-native';
import auth from '@react-native-firebase/auth';

import { name as appName } from './app.json';
import Login from './Views/Login/Login';
import Navigation from './Views/Navigation/Navigation';

const App = () => { return RenderApp() };
function RenderApp() {
	const [initializing, setInitializing] = useState(true);
	const [user, setUser] = useState();

  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; 
  }, []);

  if (initializing) return null;

   if (!user) {
    return (<Login />);
  }

  return (<Navigation />);
}

AppRegistry.registerComponent(appName, () => App);
